package res.zillennium.com.realestate.utilty;

/**
 * Created by K.TOLA on 4/24/18.
 * Phone: 017-847-800
 * E-mail: kuntola883@gmail.com
 */

public class StringFormatter {

    public static String toDollar(String value) {
        //value = "1234567.90";
        String[] data = value.split("\\.");

        try {
            String beforeDot = "";
            String afterDot = ".00";

            if (data.length > 1)
                afterDot = "." + data[1];

            Integer first = Integer.parseInt(data[0]);
            beforeDot = first.toString();

            String newValue = "";
            int i = 0, splitPosition = 0;
            for (i = beforeDot.length() - 1; i >= 0; i--) {
                newValue = beforeDot.charAt(i) + newValue;
                splitPosition++;
                if (splitPosition % 3 == 0)
                    newValue = "," + newValue;
            }
            value = newValue + afterDot;

            if (value.charAt(0) == ',')
                value = value.substring(1, value.length());

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        return "$" + value;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

}
